<?php namespace Base33\ExtendedBuilder\Controllers;

use Winter\Builder\controllers\Index as BuilderIndex;
use Base33\ExtendedBuilder\Widgets\PluginList;
use Base33\ExtendedBuilder\Widgets\ModelList;
use BackendMenu;

/**
 * ExtendedBuilder index controller
 *
 */
class Index extends BuilderIndex
{

    /**
     * Constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Base33.ExtendedBuilder', 'autofields', 'models');

        $this->bodyClass = 'compact-container ext-builder';
        $this->pageTitle = 'Base33.extendedbuilder::lang.plugin.name';
        $this->addCss('/plugins/base33/extendedbuilder/assets/css/custom.css', 'Base33.ExtendedBuilder');
        
        new PluginList($this, 'pluginList');
        new ModelList($this, 'modelList');
    }
}
