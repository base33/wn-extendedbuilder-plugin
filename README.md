# WN Extended Builder Plugin
Form fields auto-generator that extends the Winter CMS Winter Builder plugin.

## About This Plugin
This plugin extends the Winter CMS Winter Builder plugin. Save time with form creation by auto-generating a basic YAML form with fields batch-generated from the model table columns. Thereafter you can configure and customize the form using Winter Builder.

## Requirements
- Winter Builder (https://github.com/wintercms/wn-builder-plugin)

## Usage
- Install the plugin.
- Navigate to **Auto Fields** on the menu
- Select your plugin
- Select the model
- Set the field options for each table column
- Click on **GENERATE FORM**
- Open the generated YAML form in **Winter Builder** and configure/customize it.
