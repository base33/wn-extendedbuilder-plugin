<?php namespace Base33\ExtendedBuilder\Widgets;

use Winter\Builder\Widgets\PluginList as BuilderPluginList;

/**
 * Plugin list widget.
 * @extends winter\builder
 */
class PluginList extends BuilderPluginList 
{
    
}